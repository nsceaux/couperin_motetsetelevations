\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Dialogus inter Jesum et hominem"
  subtitle = "à 2 voix"
  poet = \sourceMotets
}
\includeScore "DialogusInterJesumEtHominem/A"
\includeScore "DialogusInterJesumEtHominem/B"
\includeScore "DialogusInterJesumEtHominem/C"
\includeScore "DialogusInterJesumEtHominem/D"
\includeScore "DialogusInterJesumEtHominem/E"
