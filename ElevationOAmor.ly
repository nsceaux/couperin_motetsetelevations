\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "O Amor"
  subtitle = "Élévation à 3 voix"
  poet = \sourceElevations
}
\includeScore "ElevationOAmor/A"
\includeScore "ElevationOAmor/B"
\includeScore "ElevationOAmor/C"
\includeScore "ElevationOAmor/D"
\includeScore "ElevationOAmor/E"
