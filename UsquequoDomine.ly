\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Usquequo Domine"
  subtitle = "à voix seule"
  poet = \sourceMotets
}
\includeScore "UsquequoDomine/A"
\includeScore "UsquequoDomine/B"
