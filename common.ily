\header {
  copyrightYear = "2007"
  composer = "François Couperin"

  sourceMotets = \markup\override #'(baseline-skip . 2) \column\tiny {
    \line { Source : \italic { François Couperin, Mottets [sic], } }
    \line\italic { a voix seule, deux et trois parties et symphonies, }
    \line { copie de l'atelier Philidor, Ca 1705, }
    \with-url #"https://gallica.bnf.fr/ark:/12148/bpt6k109665g/f2"
    \line { BnF, Centre technique du livre, RES-F-1679. }
  }
  sourceElevations = \markup\override #'(baseline-skip . 2) \column\tiny {
    \line { Source : \italic { François Couperin, Elévations et motets } }
    \line\italic { à une, deux et trois voix avec ou sans dessus et basse }
    \with-url #"https://bibliotheque.versailles.fr/detail-d-une-notice/notice/945185886-7809"
    \line { Bibliothèque de Versailles, Manuscrit musical 59, Philidor. }
  }
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef #t)

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size 18)

\paper {
  #(define page-breaking ly:optimal-breaking)
}

\language "italiano"
\include "nenuvar-lib.ily"
\opusTitle "Couperin – Motets et élévations"

\layout {
  indent = \smallindent
  short-indent = #0
  ragged-last = ##f
}

#(define-markup-command (title-motet layout props title subtitle)
                        (string? string?)
  (interpret-markup layout props
   (markup #:column (#:vspace 2
                     #:fill-line (#:fontsize 6 title)
                     #:vspace 1
                     #:fill-line (#:fontsize 1 subtitle)
                     #:vspace 1))))

motet =
#(define-music-function (parser location name title subtitle) (string? string? string?)
  (*path* name)
  (add-page-break parser)
  (add-toc-item parser 'tocPieceMarkup title)
  (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
  (add-odd-page-header-text parser (string-upper-case title) #f)
  (add-toplevel-markup parser (markup #:title-motet title subtitle))
  (add-no-page-break parser)
  (make-music 'Music 'void #t))
