\score {
  \notemode <<
    \new ChoirStaff \transpose do mi <<
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/A/basse-taille1"
        \clef "varbaritone/G_8"
      >> \includeLyrics "../../PrecatioAdDeum/A/paroles1"
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/A/basse-taille2"
      >> \includeLyrics "../../PrecatioAdDeum/A/paroles2"
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/A/basse"
      >> \includeLyrics "../../PrecatioAdDeum/A/paroles3"
      \new Staff <<
        \global \includeNotes "../../PrecatioAdDeum/A/basse-continue"
        \includeFigures "../../PrecatioAdDeum/A/chiffres"
      >>
    >>
  >>
  \layout { }
  \midi { \context { \Score tempoWholesPerMinute = #(ly:make-moment 200 4) } }
}
