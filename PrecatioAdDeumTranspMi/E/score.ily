\score {
  \notemode <<
    \new ChoirStaff \transpose do mi <<
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/E/basse-taille1"
        \clef "varbaritone/G_8"
      >> \includeLyrics "../../PrecatioAdDeum/E/paroles1"
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/E/basse-taille2"
      >> \includeLyrics "../../PrecatioAdDeum/E/paroles2"
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/E/basse"
      >> \includeLyrics "../../PrecatioAdDeum/E/paroles3"
      \new Staff <<
        \global \includeNotes "../../PrecatioAdDeum/E/basse-continue"
        \includeFigures "../../PrecatioAdDeum/E/chiffres"
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { \context { \Score tempoWholesPerMinute = #(ly:make-moment 160 4) } }
}
