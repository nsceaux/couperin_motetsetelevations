\score {
  \notemode <<
    \new ChoirStaff \transpose do mi <<
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/B/basse-taille"
        { s1*5 s4 \clef "varbaritone/G_8" }
      >> \includeLyrics "../../PrecatioAdDeum/B/paroles"
      \new Staff <<
        \global \includeNotes "../../PrecatioAdDeum/B/basse-continue"
        \includeFigures "../../PrecatioAdDeum/B/chiffres" >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { \context { \Score tempoWholesPerMinute = #(ly:make-moment 80 4) } }
}
