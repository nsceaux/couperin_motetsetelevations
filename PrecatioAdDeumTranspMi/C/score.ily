\score {
  \notemode <<
    \new ChoirStaff \transpose do mi <<
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/C/basse-taille1"
        \clef "varbaritone/G_8"
      >> \includeLyrics "../../PrecatioAdDeum/C/paroles1"
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/C/basse-taille2"
      >> \includeLyrics "../../PrecatioAdDeum/C/paroles2"
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/C/basse"
      >> \includeLyrics "../../PrecatioAdDeum/C/paroles3"
      \new Staff <<
        \global \includeNotes "../../PrecatioAdDeum/C/basse-continue"
        \includeFigures "../../PrecatioAdDeum/C/chiffres" >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { \context { \Score tempoWholesPerMinute = #(ly:make-moment 80 4) } }
}
