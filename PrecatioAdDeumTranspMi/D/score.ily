\score {
  \notemode <<
    \new ChoirStaff \transpose do mi <<
      \new Staff \withLyrics <<
        \global \includeNotes "../../PrecatioAdDeum/D/basse"
      >> \includeLyrics "../../PrecatioAdDeum/D/paroles"
      \new Staff <<
        \global \includeNotes "../../PrecatioAdDeum/D/basse-continue"
        \includeFigures "../../PrecatioAdDeum/D/chiffres"
      >>
    >>
  >>
  \layout { indent = \noindent }
  \midi { \context { \Score tempoWholesPerMinute = #(ly:make-moment 160 4) } }
}
