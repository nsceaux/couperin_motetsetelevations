\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Precatio ad Deum"
  subtitle = "à 3 voix"
  poet = \sourceMotets
}
\includeScore "PrecatioAdDeum/A"
\includeScore "PrecatioAdDeum/B"
\includeScore "PrecatioAdDeum/C"
\includeScore "PrecatioAdDeum/D"
\includeScore "PrecatioAdDeum/E"
