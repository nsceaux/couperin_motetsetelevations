\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Salve Regina"
  subtitle = "à voix seule"
  poet = \sourceMotets
}
\includeScore "SalveRegina/A"
\includeScore "SalveRegina/B"
\includeScore "SalveRegina/C"
\includeScore "SalveRegina/D"
