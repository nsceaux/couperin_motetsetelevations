\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Magnificat"
  subtitle = "à 2 voix"
  poet = \sourceMotets
}
\includeScore "Magnificat/A"
\includeScore "Magnificat/B"
\includeScore "Magnificat/C"
\includeScore "Magnificat/D"
\includeScore "Magnificat/E"
