\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Aspiratio mentis ad Deum"
  subtitle = "à 2 voix"
  poet = \sourceMotets
}
\includeScore "AspiratioMentisAdDeum/A"
\includeScore "AspiratioMentisAdDeum/B"
\includeScore "AspiratioMentisAdDeum/C"
\includeScore "AspiratioMentisAdDeum/D"
\includeScore "AspiratioMentisAdDeum/E"
\includeScore "AspiratioMentisAdDeum/F"
