\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Ad te levavi oculos meos"
  subtitle = "à voix et symphonie"
  poet = \sourceMotets
}
\includeScore "AdTeLevaviOculosMeos/A"
\includeScore "AdTeLevaviOculosMeos/B"
\includeScore "AdTeLevaviOculosMeos/C"
