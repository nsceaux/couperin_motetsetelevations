\score {
  \new StaffGroupNoBar <<
    \new Staff <<
      { \beginMark Symphonie }
      \global \includeNotes "dessus"
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \includeNotes "voix-basse"
    >> \includeLyrics "paroles"
    \new Staff << \global \includeNotes "basse"
                 \includeFigures "chiffres" >>
  >>
  \layout { }
  \midi { \context { \Score tempoWholesPerMinute = #(ly:make-moment 210 4) } }
}
