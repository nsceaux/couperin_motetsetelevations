\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "O misterium ineffabile"
  subtitle = "Élévation à 2 voix"
  poet = \sourceElevations
}
\includeScore "ElevationOMisteriumIneffabile/A"
\includeScore "ElevationOMisteriumIneffabile/B"
\includeScore "ElevationOMisteriumIneffabile/C"
\includeScore "ElevationOMisteriumIneffabile/D"
