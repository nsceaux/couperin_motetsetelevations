\version "2.23.4"
\include "common.ily"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\layout { \context { \Score \override VerticalAlignment #'max-stretch = ##f } }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \header {
    title = \markup \column {
      \fill-line { "Motets à voix seule," }
      \fill-line { "deux et trois voix" }
    }
  }

  \paper { #(define page-breaking ly:minimal-breaking) }
  %% Title page
  \markup \null
  \pageBreak

  %% Table of contents
  \markuplist
  \with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #f)
  \override-lines #'(column-number . 1)
  \table-of-contents


  %% notes
  
  \markuplist\with-line-width-ratio#0.7 {
    \vspace#5
    \bold Sources
    \vspace #1
    \wordwrap {
      François Couperin, Mottets [sic], a voix seule, deux et trois parties 
      et symphonies, copie de l'atelier Philidor, Ca 1705,
      \with-url #"https://gallica.bnf.fr/ark:/12148/bpt6k109665g"
      \line { BnF, département Centre technique du livre, RES-F-1679. }
    }
    \vspace #1
    \wordwrap {
      François Couperin, Elévations et motets à une, deux et trois voix avec ou sans
      dessus et basse : Mf n° 35
      \with-url #"https://bibliotheque.versailles.fr/detail-d-une-notice/notice/945185886-7809" \line { Bibliothèque de Versailles, Manuscrit musical 59, Philidor }

    }
  }
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%
\bookpart {
  \motet "AspiratioMentisAdDeum"
  "Aspiratio mentis ad Deum" "Haute-contre, taille, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
  \includeScore "E"
  \includeScore "F"
}

%%%
\bookpart {
  \motet "DialogusInterJesumEtHominem"
  "Dialogus inter Jesum et hominem" "Haute-contre, basse, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
  \includeScore "E"
}

%%%
\bookpart {
  \motet "SalveRegina"
  "Salve Regina" "Haute-contre, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
}

%%%
\bookpart {
  \motet "SalvumMeFacDeus"
  "Salvum me fac Deus" "Basse, basse continue et symphonie"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
  \includeScore "E"
  \includeScore "F"
  \includeScore "G"
  \includeScore "H"
  \includeScore "I"
  \includeScore "J"
}

%%%
\bookpart {
  \motet "PrecatioAdDeum"
  "Precatio ad Deum" "Deux basse-tailles, basse, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
  \includeScore "E"
}

%%%
\bookpart {
  \motet "UsquequoDomine"
  "Usquequo Domine" "Haute-contre, basse continue"
  \includeScore "A"
  \includeScore "B"
}

%%%
\bookpart {
  \motet "Magnificat"
  "Magnificat" "Deux dessus, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
  \includeScore "E"
}

%%%
\bookpart {
  \motet "AdTeLevaviOculosMeos"
  "Ad te levavi oculos meos" "Basse, basse continue et deux dessus"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
}

%%%
\bookpart {
  \motet "ElevationOMisteriumIneffabile"
  "O misterium ineffabile" "Dessus, basse, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
}

%%%
\bookpart {
  \motet "ElevationOAmor"
  "O Amor" "Haute-contre, taille, basse, basse continue"
  \includeScore "A"
  \includeScore "B"
  \includeScore "C"
  \includeScore "D"
  \includeScore "E"
}
