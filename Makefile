OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH:=$(shell pwd)/../../nenuvar-lib
LILYPOND_OPTIONS=--loglevel=WARN -ddelete-intermediate-files -dno-protected-scheme-parsing
LILYPOND_CMD=lilypond -I$(shell pwd) -I$(NENUVAR_LIB_PATH) $(LILYPOND_OPTIONS)

#conducteur:
#	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motets et élévations" main.ly
#.PHONY: conducteur

AspiratioMentisAdDeum:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Aspiratio mentis ad Deum" $@.ly
.PHONY: AspiratioMentisAdDeum

DialogusInterJesumEtHominem:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Dialogus inter Jesum et hominem" $@.ly
.PHONY: DialogusInterJesumEtHominem

SalveRegina:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Salve Regina" $@.ly
.PHONY: SalveRegina

SalvumMeFacDeus:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Salvum me fac Deus" $@.ly
.PHONY: SalvumMeFacDeus

PrecatioAdDeum:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Precatio ad Deum" $@.ly
.PHONY: PrecatioAdDeum

UsquequoDomine:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Usquequo Domine" $@.ly
.PHONY: UsquequoDomine

Magnificat:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Magnificat" $@.ly
.PHONY: Magnificat

AdTeLevaviOculosMeos:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Motet - Ad te levavi oculos meos" $@.ly
.PHONY: AdTeLevaviOculosMeos

ElevationOMisteriumIneffabile:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Élévation - O misterium ineffabile" $@.ly
.PHONY: ElevationOMisteriumIneffabile

ElevationOAmor:
	$(LILYPOND_CMD) -o "$(OUTPUT_DIR)/Couperin - Élévation - O Amor" $@.ly
.PHONY: ElevationOAmor

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@mv -fv $(OUTPUT_DIR)/*.pdf $(DELIVERY_DIR)

clean:
	@rm -f $(OUTPUT_DIR)/*

all: check \
	AspiratioMentisAdDeum \
	DialogusInterJesumEtHominem \
	SalveRegina \
	SalvumMeFacDeus \
	PrecatioAdDeum \
	UsquequoDomine \
	Magnificat \
	AdTeLevaviOculosMeos \
	ElevationOMisteriumIneffabile \
	ElevationOAmor \
	delivery clean

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: clean all check delivery
