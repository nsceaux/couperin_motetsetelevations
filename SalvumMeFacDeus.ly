\version "2.23.4"
\include "common.ily"

\paper {
  bookTitleMarkup = \shortBookTitleMarkup
}
\header {
  title = "Salvum me fac Deus"
  subtitle = "à voix seule et symphonie"
  poet = \sourceMotets
}
\includeScore "SalvumMeFacDeus/A"
\includeScore "SalvumMeFacDeus/B"
\includeScore "SalvumMeFacDeus/C"
\includeScore "SalvumMeFacDeus/D"
\includeScore "SalvumMeFacDeus/E"
\includeScore "SalvumMeFacDeus/F"
\includeScore "SalvumMeFacDeus/G"
\includeScore "SalvumMeFacDeus/H"
\includeScore "SalvumMeFacDeus/I"
\includeScore "SalvumMeFacDeus/J"
